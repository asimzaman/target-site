import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Navbar from './components/Layout/Navbar/Navbar'


const useStyles = makeStyles(theme => ({
  root: {
    // minHeight: '100vh',
  },
 
}));

const App = () => {
  const classes = useStyles(); 
  return (
    <div className={classes.root}>
    <Router>
      <Navbar />
    </Router>
    </div>
  );
}

export default App;
